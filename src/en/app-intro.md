# Create an application

The following chapters form a tutorial that will guide you in the development of a simple application with Guile.

Like the first chapters, I propose to approach the exercise through development practices and methods that I appreciate a lot (Test Driven Development, Clean Code, Clean Architecture).

I'll try to give you the essential knowledge to move forward and where to find more information (if you want to go further).

The first part consists in creating the core of the application. This is not the way I usually work but it will provide the foundation for the next part. The idea is to get you quickly to a point where your program works. After that, you will set up version tracking, documentation, and distribute the application with a package manager.

The second part will deal with the development of the interfaces of our application: command line, web, and graphical!

> **Prerequisites:** This tutorial does not replace an introduction to programming and expects you to be familiar with some basic concepts. You should be comfortable using the command line. If you already know how to program in other languages, this tutorial can be a good first contact with Guile.
>
> **Getting help:** If at any time you feel overwhelmed or confused by a feature you are using, take a look at [the official language reference](https://www.gnu.org/software/guile/manual/html_node/index.html). It is distributed with most Guile installations (`info guile`).

So, what kind of project are you going to create? See you in the next chapter to find out!

> Note: This tutorial is written for Guile 3. The code examples can be used in any version of Guile 3.

