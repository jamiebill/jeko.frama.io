# Emacs

[Emacs](https://www.gnu.org/software/emacs/) is the editor that provides the best support for Guile.

## Installation

You can install it the same way you installed Guile (below, the example with Guix) :

```bash
$ guix install emacs
```

## Configuration

This book is not intended to teach how to use Emacs. So I propose you hereafter a ready-to-use configuration to make writing programs in Guile more pleasant with Emacs. Feel free to take only what you are interested in!

### Visual support

#### Highlight cursor line

```lisp
;; paste this lines into your emacs config file
(global-hl-line-mode +1)
```

Without

![Without global-hl-line-mode](images/without-current-line-highlight.gif)

With

![With global-hl-line-mode](images/with-current-line-highlight.gif)

#### Highlight the pair of delimiters under the cursor

```lisp
;; paste this lines into your emacs config file
(show-paren-mode 1)
(setq show-paren-delay 0)
```

Without

![Without show-paren-mode](images/without-show-parens.gif)

With

![With show-paren-mode](images/with-show-parens.gif)

### Operational support

#### " make Scheme hacking inside Emacs (even more) fun "

If you only had to install one extension, this is it.

*Installation*

```bash
$ guix install emacs-geiser emacs-geiser-guile
```

*Activation*

```
M-x run-guile
```

*Usage*

You can refer to [https://www.nongnu.org/geiser/geiser_5.html#Cheat-sheet](https://www.nongnu.org/geiser/geiser_5.html#Cheat-sheet)

#### Add auto-completion to Geiser :

*Installation*

```bash
$ guix install emacs-ac-geiser
```

*Activation*

```lisp
;; paste this lines into your emacs config file
(ac-config-default)
(require 'ac-geiser)
(add-hook 'geiser-mode-hook 'ac-geiser-setup)
(add-hook 'geiser-repl-mode-hook 'ac-geiser-setup)
(eval-after-load "auto-complete")
  (add-to-list 'ac-modes' geiser-repl-mode))
```

Without

![Without ac-geiser](images/without-ac-geiser.gif)

With

![With ac-geiser](images/with-ac-geiser.gif)

#### Edit the code based on the S-expression structure

*Installation*

```bash
$ guix install emacs-paredit
```

*Activation*

```lisp
;; paste this lines into your emacs config file
(require 'paredit)
(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'scheme-mode-hook #'enable-paredit-mode)
```

*Usage*

I couldn't do better than [this guide by Dan Midwood](http://danmidwood.com/content/2014/11/21/animated-paredit.html).

### Refactoring support

#### Edit several strings of characters in the same way simultaneously

*Installation*

```bash
$ guix install emacs-iedit
```
*Activation*

```lisp
;; paste this lines into your emacs config file
(require 'iedit)
```

*Usage*

`C-;` on a word to edit all its occurrences.  
`C-0 C-;` on a word to edit all its occurrences in the active region.

#### Edit several places in the file in the same way simultaneously.

 *Installation*

```bash
$ guix install emacs-multiple-cursors
```

*Activation*

```lisp
;; paste this lines into your emacs config file
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
```

*Usage*

`C->` Add a cursor on the next line  
`C-<` Add a cursor on the previous line

When editing is finished, `C-g`.

#### Extract variables and functions


*Installation*

```lisp
;; paste this lines into your emacs config file
(if (not (package-installed-p 'emr))
    (progn
      (package-refresh-contents)
      (package-install 'emr))))
```

*Activation*

```lisp
;; paste this lines into your emacs config file
(require 'emr)
(autoload 'emr-show-refactor-menu "emr")
(define-key prog-mode-map (kbd "M-RET") 'emr-show-refactor-menu)
(eval-after-load "emr" '(emr-initialize))
(global-set-key (kbd "M-v") 'emr-scm-extract-variable)
(global-set-key (kbd "M-f") 'emr-scm-extract-function)
```

*Usage*

`M-v` Extract a variable  
`M-f` Extract a function
