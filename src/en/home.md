![](https://www.gnu.org/software/guile/static/base/img/kid-programming-a-robot.png)

# Background

Learning a new programming language is a tedious undertaking. In my case, I found the beginning of my learning of the Guile language laborious.

* I started with no experience in Lisp or its dialects (like Scheme).
* I didn't know anyone experienced (or even willing to learn) in Lisp or its dialects.
* I am not a researcher.
* Mathematics is not my vocation.
* I like to tinker to learn, to follow a tutorial to develop a small simple software, to read blog articles, to watch a live-coding video... 

In short, at first sight, Guile and me, it was not made to stick.

In the **Guile Hacker's Notebook**, I want to gather everything I think an apprentice hacker would need to go from beginner (discovering the syntax) to intermediate (creating simple programs). 

> This is the resource I wish I had found when I started.

## A complement to the Scheme language literature

Lisp is one of the oldest families of programming languages.

Scheme is a dialect of the Lisp programming language (yes, it is named after its family). The Scheme language specification evolves through revisions (not all of which are agreed upon by the entire Scheme community).

Guile is an implementation of the Scheme language.

Thus, the resources to learn Guile are mixed with those to learn Scheme or even Lisp. But they diverge in terms of features/api/modules.

This makes the learning experience uncomfortable when one finds a resource illustrating a Scheme concept with a few lines of code that Guile's REPL can't interpret.

The **Guile Hacker's Notebook** is a resource dedicated specifically to Guile. All functions/modules used in this book are part of the Guile distribution.

## A complement to the Guile language reference

As its name indicates, a reference is a book that is regularly returned to with a specific need. It provides the exhaustive list of the language features without explaining how to implement them (it happens but it is not systematic).

The **Guile Hacker's Notebook** illustrates these concepts by structured and concrete situations.

## What do you need

- A computer and the GNU/Linux system of your choice. 
- Some programming experience.
- The desire to learn.

## Feedbacks

Please, help me improve this book! Give me all your comments:
- did I skip a step?
- any inconsistencies?
- need more information?
- something doesn't work?
- spelling mistakes?
- not enough love?
- You dig it?

Send it all to me:
- [email](mailto:jeremy@korwin-zmijowski.fr)
- [twitter](https://twitter.com/JeremyKorwin)
- [linkedin](www.linkedin.com/in/jeko)
- [mastodon](@jeko@framapiaf.org)
- [instagram](https://www.instagram.com/jeremy_kz/)
- by opening ticket on the [repository](https://framagit.org/Jeko/jeko.frama.io)

*Inspired by : quii.gitbook.io*
