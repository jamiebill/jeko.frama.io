![](https://www.gnu.org/software/guile/static/base/img/kid-programming-a-robot.png)

# Contexte

Apprendre un nouveau langage de programmation est une entreprise fastidieuse. En ce qui me concerne, j'ai trouvé le début de mon apprentissage du langage Guile laborieux.

* J'ai démarré sans expérience en Lisp ou ses dialectes (comme Scheme).
* Je ne connaissais personne d'expérimenté (ou même désireux d'apprendre) en Lisp ou ses dialectes.
* Je ne suis pas un chercheur.
* Les mathématiques ne sont pas ma vocation.
* J'aime bidouiller pour apprendre, suivre un tutoriel pour développer un petit logiciel simple, lire des articles de blog, regarder une vidéo de live-coding… 

Bref, à première vue, Guile et moi, ce n'était pas fait pour coller.

Dans le **Carnet du hacker Guile**, je veux rassembler tout ce dont un apprenti hacker aurait, selon moi, besoin pour passer du stade de débutant (qui découvre la syntaxe) à celui d'intermédiaire (qui créé des programmes simples). 

> C'est la ressource que j'aurais voulu trouver quand j'ai démarré.

## Un complément à la littérature du langage Scheme

Lisp est une des plus vielles familles de langage de programmation.

Scheme est un dialecte du langage de programmation Lisp (oui, il porte le nom de sa famille). La spécification du langage Scheme évolue au rythme de révisions (chacune de ces révisions ne faisant pas consensus dans la toute la communauté Scheme).

Guile est une implémentation du langage Scheme.

Ainsi, les ressources pour apprendre Guile se mêlent avec celles pour apprendre Scheme, voire Lisp. Mais elles divergent en en termes de fonctionnalités/api/modules.

Cela rend l'expérience d'apprentissage inconfortable lorsqu'on trouve une ressource illustrant un concept de Scheme avec quelques lignes de code que le REPL de Guile ne peut pas interpréter.

Le **Carnet du hacker Guile** est une ressource dédiée spécifiquement à Guile. Tou(te)s les fonctions/modules utilisés dans cet ouvrage font parti de la distribution de Guile.

## Un complément à la référence du langage Guile

Comme son nom l'indique, une référence est un ouvrage sur lequel on revient régulièrement avec un besoin bien précis. Elle fourni la liste exhaustive des fonctionnalités du langage sans pour autant expliquer comment les mettre en oeuvre (ça arrive mais ce n'est pas systématique).

Le **Carnet du hacker Guile** illustre ces concepts par des mises en situations structurées et concrètes.

## De quoi tu auras besoin

- Un ordinateur et le système GNU/Linux de ton choix. 
- Un peu d'expérience en programmation.
- L'envie d'apprendre.

## Retours

S'il te plait, aide-moi à améliorer cet ouvrage ! Fais moi part de toutes tes remarques :
- est-ce que j'ai sauté une étape ?
- des incohérences ?
- besoin d'information supplémentaires ?
- quelque chose ne fonctionne pas ?
- des fautes d'orthographe ?
- pas assez d'amour ?
- tu kiff à donf ?

Envoies-moi tout ça :
- [email](mailto:jeremy@korwin-zmijowski.fr)
- [twitter](https://twitter.com/JeremyKorwin)
- [linkedin](www.linkedin.com/in/jeko)
- [mastodon](@jeko@framapiaf.org)
- [instagram](https://www.instagram.com/jeremy_kz/)
- en ouvrant un ticket sur le [dépôt](https://framagit.org/Jeko/jeko.frama.io)

*Inspiré par : quii.gitbook.io*
