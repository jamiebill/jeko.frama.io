# Créer une application

Les chapitres qui vont suivre forment un tutoriel qui te guidera dans le développement d'une application simple avec Guile.

À l'instar des premiers chapitres, je te propose d'aborder l'exercice à travers des pratiques et méthodes de développement que j'apprécie beaucoup (Test Driven Development, Clean Code, Clean Architecture).

J'essayerai de te transmettre l'essentiel des connaissances pour avancer et où trouver plus d'informations (si tu veux aller plus loin).

La première partie consiste à créer le coeur de l'application. Ce n'est pas ainsi que j'ai l'habitude de travailler mais cela apportera le socle sur lequel reposera la partie suivante. L'idée est de t'amener rapidement à un point où ton programme fonctionne. Après quoi, tu mettras en place un suivi de version, de la documentation, et de quoi distribuer l'application à l'aide d'un gestionnaire de paquet.

La deuxième partie traitera du développemet des interfaces de notre application : en ligne de commande, web, et graphique !

> **Prérequis :** ce tutoriel ne remplace pas une introduction à la programmation et s'attend à ce que tu sois familier avec quelques concepts de base. Tu dois être à l'aise avec l'utilisation de la ligne de commande. Si tu sais déjà programmer dans d'autres languages, ce tutoriel peut être un bon premier contact avec Guile.
>
> **Obtenir de l'aide :** si, à tout instant, tu te sens dépassé ou confus par une fonctionnalité utilisée, jette un oeil à [la référence officielle du langage](https://www.gnu.org/software/guile/manual/html_node/index.html). Elle est distribuée avec la plupart des installations de Guile (`info guile`).

Alors, quel type de projet vas tu créer ? Rendez-vous dans le prochain chapitre pour le savoir !

> **Note :** ce tutoriel est écrit pour Guile 3. Les exemples de code peuvent être utilisés dans toute versions de Guile 3.
